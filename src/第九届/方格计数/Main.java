package 第九届.方格计数;

/**
 * 我们以某个小方格的一个顶点为圆心画一个半径为1000的圆。
 * 你能计算出这个圆里有多少个完整的小方格吗？
 * 1998x1998=3992004
 */

/**
 * 使用半径进行计算，计算1/4的圆的方格3137548
 */
public class Main {
    public static void main(String[] args) {
//        System.out.println(1998*1998);
        int count=0,n=1000;
        for(int i=1;i<=n;i++){
            for(int j=1;j<=n;j++){
                if(i*i+j*j<=n*n){
                    count++;
                }
            }
        }
        System.out.println(count*4);
    }
}
