package 算法模板;


import static java.lang.Math.pow;

public class 全排列_模板1 {
    public static void main(String[] args) {
//        dfs(0);
//        System.out.println(ans); 362880
//        ans= 1*2*3*4*5*6*7*8*9; //362880
        System.out.println(ans);
    }
    static int[] a=new int[]{1,2,3,4,5,6,7,8,9};
    static int n=9,ans=0;
    private static void dfs(int m) {
        if(m>=n){
            System.out.println("ans:"+ans);
            ans++;
            for(int i=0;i<n;i++){
                System.out.println(a[i]+" ");
            }
            System.out.println();
            return;
        }
        for(int i=m;i<n;i++){
            swap(i,m);
            dfs(m+1);
            swap(i,m); //回溯
        }
    }

    private static void swap(int i, int m) {
        int t=a[i];
        a[i]=a[m];
        a[m]=t;
    }
}
