package 第六届.三羊献瑞;

/**
 * 观察下面的加法算式：
 *
 *       祥 瑞 生 辉
 *   +   三 羊 献 瑞
 * -------------------
 *    三 羊 生 瑞 气
 *
 * (如果有对齐问题，可以参看【图1.jpg】)
 *
 * 其中，相同的汉字代表相同的数字，不同的汉字代表不同的数字。
 *
 * 请你填写“三羊献瑞”所代表的4位数字（答案唯一），不要填写任何多余内容。
 * ————————————————
 **/

/**
 * 相同的汉字代表相同的数字，不同的汉字代表不同的数字。
 * 1085
 */
public class Main {
    public static void main(String[] args) {
        for(int a=1;a<=9;a++){
            for(int b=0;b<=9;b++){
                if(b==a) continue;
                for(int c=0;c<=9;c++){
                    if(b==c||a==c) continue;
                    for(int d=0;d<=9;d++){
                        if(b==d||d==c||d==a) continue;
                        for(int e=1;e<=9;e++){
                            if(b==e||e==d||e==c||e==a) continue;
                            for(int f=0;f<=9;f++){
                                if(b==f||a==f||c==f||d==f||e==f) continue;
                                for(int g=0;g<=9;g++){
                                    if(b==g||a==g||c==g||d==g||e==g||f==g) continue;
                                    for(int h=0;h<=9;h++){
                                        if(b==h||a==h||c==h||d==h||e==h||f==h||g==h) continue;
                                        int x=a*1000+b*100+c*10+d;
                                        int y=e*1000+f*100+g*10+b;
                                        int z=e*10000+f*1000+c*100+b*10+h;
//                                        System.out.println(y);
                                        if(x+y==z){
                                            System.out.println(y);
                                            return;
                                        }
                                    }

                                }

                            }
                        }
                    }

                }

            }
        }
    }
}
