package 第八届.纸牌三角形;

/**
 * 在一个长度为 n 的数组 nums 里的所有数字都在 0～n-1 的范围内。数组中某些数字是重复的，
 * 但不知道有几个数字重复了，也不知道每个数字重复了几次。请找出数组中任意一个重复的数字。
 *
 * 输入：
 * [2, 3, 1, 0, 2, 5, 3]
 * 输出：2 或 3
 * 问题：请给出这个题目能想到的所有解法，并分析每种解法的时间复杂度和空间复杂度
 */

/**
 * 1.将该数与位置上的数交换，遍历一次
 */

public class Main {
    public static void main(String[] args) {
        dfs(0);
        System.out.println(ans/6.0);
    }
    static int[] a=new int[]{1,2,3,4,5,6,7,8,9};
    static int ans=0;
    static void dfs(int m){
        if(m>=9){
            if(a[0]+a[1]+a[3]+a[5]==(a[0]+a[2]+a[4]+a[8]) && (a[0]+a[1]+a[3]+a[5])==a[5]+a[6]+a[7]+a[8]){
                ans++;
            }
            return;
        }
        for(int i=m;i<9;i++){
            swap(i,m);
            dfs(m+1);
            swap(i,m);
        }
    }
    static void swap(int i,int j){
        int t=a[i];
        a[i]=a[j];
        a[j]=t;
    }




    //        int[] nums={2, 3, 1, 0, 4, 5, 6};
//        for(int i=0;i<nums.length;i++){
//            while(nums[i]!=i){
//                for(int j=0;j<nums[i];j++){
//                    int tmp=nums[nums[i]];
//                    nums[nums[i]]=nums[i];
//                    nums[i]=tmp;
//                }
//            }
//            System.out.println(i);
//            return;
//
//        }

//    static class ListNode{
//        ListNode next;
//        int val;
//        ListNode(int val){
//            this.val=val;
//        }
//    }
//    public static void main(String[] args) {
//        ListNode head=new ListNode(1);
//        int k=-3;
//        ListNode node1=new ListNode(2);
//        head.next=node1;
//        ListNode node2=new ListNode(3);
//        node1.next=node2;
//        ListNode node3=new ListNode(4);
//        node2.next=node3;
//        ListNode node4=new ListNode(5);
//        node3.next=node4;
//        ListNode pre,cur;
//        pre=cur=head;
//        if(k<=0) {
//            System.out.println("输入错误");
//            return;
//        }
//        while((k--)>0){
//            cur=cur.next;
//        }
//        while(cur!=null){
//            pre=pre.next;
//            cur=cur.next;
//        }
//        System.out.println(pre.val);
//    }
}
