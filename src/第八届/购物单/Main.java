package 第八届.购物单;

/**
 * 小明刚刚找到工作，老板人很好，只是老板夫人很爱购物。老板忙的时候经常让小明帮忙到商场代为购物。小明很厌烦，但又不好推辞。
 *
 *   这不，XX大促销又来了！老板夫人开出了长长的购物单，都是有打*0.01优惠的。
 *   小明也有个怪癖，不到万不得已，从不刷卡，直接现金搞定。
 *   现在小明很心烦，请你帮他计算一下，需要从取款机上取多少现金，才能搞定这次购物。
 *
 *   取款机只能提供100元面额的纸币。小明想尽可能少取些现金，够用就行了。
 *   你的任务是计算出，小明最少需要取多少现金。
 *
 * 以下是让人头疼的购物单，为了保护隐私，物品名称被隐藏了。
 * -----------------
180.90*88*0.01
+10.25*65*0.01
+56.14*9*0.01
+104.65*9*0.01
+100.30*88*0.01
+297.15*0.5
+26.75*65*0.01
+130.62*0.5
+240.28*58*0.01
+270.62*8*0.01
+115.87*88*0.01
+247.34*95*0.01
+73.21*9*0.01
+101.00*0.5
+79.54*0.5
+278.44*7*0.01
+199.26*0.5
+12.97*9*0.01
+166.30*78*0.01
+125.50*58*0.01
+84.98*9*0.01
+113.35*68*0.01
+166.57*0.5
+42.56*9*0.01
+81.90*95*0.01
+131.78*8*0.01
+255.89*78*0.01
+109.17*9*0.01
+146.69*68*0.01
+139.33*65*0.01
+141.16*78*0.01
+154.74*8*0.01
+59.42*8*0.01
+85.44*68*0.01
+293.70*88*0.01
+261.79*65*0.01
+11.30*88*0.01
+268.27*58*0.01
+128.29*88*0.01
+251.03*8*0.01
+208.39*75*0.01
+128.88*75*0.01
+62.06*9*0.01
+225.87*75*0.01
+12.89*75*0.01
+34.28*75*0.01
+62.16*58*0.01
+129.12*0.5
+218.37*0.5
+289.69*8*0.01
 * --------------------
 *
 * 需要说明的是，88*0.01指的是按标价的88%计算，而8*0.01是按80%计算，余者类推。
 * 特别地，0.5是按50%计算。
 *
 * 请提交小明要从取款机上提取的金额，单位是元。
 * 答案是一个整数，类似4300的样子，结尾必然是00，不要填写任何多余的内容。
 *
 *
 * 特别提醒：不许携带计算器入场，也不能打开手机。
 * ————————————————
 * 版权声明：本文为CSDN博主「一叶之修」的原创文章，遵循CC 4.0 BY-SA版权协议，转载请附上原文出处链接及本声明。
 * 原文链接：https://blog.csdn.net/weixin_41793113/article/details/87976875
 */
public class Main {
    public static void main(String[] args) {
        System.out.println(180.90*88*0.01
                +10.25*65*0.01
                +56.14*9*0.1
                +104.65*9*0.1
                +100.30*88*0.01
                +297.15*0.5
                +26.75*65*0.01
                +130.62*0.5
                +240.28*58*0.01
                +270.62*8*0.1
                +115.87*88*0.01
                +247.34*95*0.01
                +73.21*9*0.1
                +101.00*0.5
                +79.54*0.5
                +278.44*7*0.1
                +199.26*0.5
                +12.97*9*0.1
                +166.30*78*0.01
                +125.50*58*0.01
                +84.98*9*0.1
                +113.35*68*0.01
                +166.57*0.5
                +42.56*9*0.1
                +81.90*95*0.01
                +131.78*8*0.1
                +255.89*78*0.01
                +109.17*9*0.1
                +146.69*68*0.01
                +139.33*65*0.01
                +141.16*78*0.01
                +154.74*8*0.1
                +59.42*8*0.1
                +85.44*68*0.01
                +293.70*88*0.01
                +261.79*65*0.01
                +11.30*88*0.01
                +268.27*58*0.01
                +128.29*88*0.01
                +251.03*8*0.1
                +208.39*75*0.01
                +128.88*75*0.01
                +62.06*9*0.1
                +225.87*75*0.01
                +12.89*75*0.01
                +34.28*75*0.01
                +62.16*58*0.01
                +129.12*0.5
                +218.37*0.5
                +289.69*8*0.1);
    }
    //5136.859500000001
    //5200
}
